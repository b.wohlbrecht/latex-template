%!TEX root = ../main.tex

% show warning for old LaTeX syntax
\RequirePackage[l2tabu, orthodox]{nag}

\documentclass[
	pdftex,
	oneside,
	12pt,				% fontsize
	parskip=half,		% Space (in lines) between paragraphs
	headheight = 12pt,	% Header hight
	headsepline,		% Line after header
	footheight = 16pt,	% Footer height
	footsepline,		% Line before footer
	abstracton,			% Abstract headline
	DIV=calc,			% Calculate print space
	BCOR=8mm,			% BCOR settings (Bindekorrektur)
	headinclude=false,	% Exclude header from print space
	footinclude=false,	% Exclude footer from print space
	listof=totoc,		% Show List of Figures/Tables in Contents
	toc=bibliography,	% Show Bibliography in Contents
]{scrreprt}				% Koma-Script report-class, long document: scrreprt, short document: scrbook

\usepackage{xstring}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

% iflang command definition
\newcommand{\iflang}[2]{
	\IfStrEq{\documentLanguage}{#1}{#2}{}
}

% ifDocType comand definition
\newcommand{\ifDocType}[3]{
	\IfStrEq{\documentType}{#1}{#2}{#3}
}

% ifMultipleAuthors definition
\newcommand{\ifMultipleAuthors}[2]{
	\IfStrEq{\multipleAuthors}{true}{#1}{#2}
}

% Include main settings
\input{settings/main}

% Include document settings
\input{settings/document}

% Load language specific Strings
\input{lang/\documentLanguage}

% Load language specific babel package
\iflang{de}{\usepackage[english, ngerman]{babel}}
\iflang{en}{\usepackage[ngerman, english]{babel}}

% Add comment feature
\newcommand{\comment}[1]{\par {\bfseries \color{blue} #1 \par}}


%%%%%%% Package Includes %%%%%%%

\usepackage[margin=\margin,foot=1cm]{geometry}
\usepackage[activate]{microtype}
\usepackage[onehalfspacing]{setspace}
\usepackage{makeidx}
\usepackage[autostyle=true,german=quotes]{csquotes}
\usepackage{longtable}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{pdfpages}
\usepackage{xcolor}
\usepackage{float}
\usepackage{array}
\usepackage{calc}
\usepackage[right]{eurosym}
\usepackage{wrapfig}
\usepackage{pgffor}
\usepackage[perpage, hang, multiple, stable]{footmisc}
\usepackage{listings}
\usepackage[obeyFinal,backgroundcolor=yellow,linecolor=black]{todonotes}
\usepackage{rotating}
\usepackage{lscape}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{\documentFont}
\usepackage[
	pdftitle={\documentTitle},
	pdfauthor={\documentAuthor},
	pdfsubject={\documentType},
	pdfcreator={pdflatex, LaTeX with KOMA-Script},
	pdfpagemode=UseOutlines,						% Show Contents while opening
	pdfdisplaydoctitle=true,						% Show document title instead of file name
	pdflang={\documentLanguage},					% Document language
]{hyperref}
\usepackage{bookmark}
\usepackage[nonumberlist,toc,acronym,nopostdot]{glossaries}
\usepackage{hyperref}
\usepackage{etoolbox}
\usepackage{xifthen}
\usepackage{underscore}
\usepackage{svg}

% Generate glossary
\usepackage{glossaries-extra}

\glsaddkey
{genitive}		% key
{}				% default value
{\glsentrygen}	% no link cs
{\Glsentrygen}	% no link ucfirst cs
{\glsgen}		% link cs
{\Glsgen}		% link ucfirst cs
{\GLSgen}		% link all caps cs

\glsaddkey
{genFirst}
{}
{\glsentrygenfOrig}
{\GlsentrygenfOrig}
{\glsgenfOrig}
{\GlsgenfOrig}
{\GLSgenfOrig}

% Append \glsunset to first use of genitive
\newcommand{\glsentrygenf}[1]{\glsentrygenfOrig{#1}\glsunset{#1}}
\newcommand{\Glsentrygenf}[1]{\GlsentrygenfOrig{#1}\glsunset{#1}}
\newcommand{\glsgenf}[1]{\glsgenfOrig{#1}\glsunset{#1}}
\newcommand{\Glsgenf}[1]{\GlsgenfOrig{#1}\glsunset{#1}}
\newcommand{\GLSgenf}[1]{\GLSgenfOrig{#1}\glsunset{#1}}

\makeglossaries{}

% Load colors
\defineColors{}

% Set Titel, Autor and Date
\title{\documentTitle}
\author{\documentAuthor}
\date{\datum}

% Set graphicx settings
\DeclareGraphicsRule{.1}{mps}{*}{}

% Floating and placeins settings for subsections
% \makeatletter
% \AtBeginDocument{
%   \expandafter\renewcommand\expandafter\subsection\expandafter{
%     \expandafter\@fb@secFB\subsection
%   }
% }
% \makeatother

% \makeatletter
% \AtBeginDocument{
%   \expandafter\renewcommand\expandafter\subsubsection\ex 	pandafter{
%     \expandafter\@fb@secFB\subsubsection
%   }
% }
% \makeatother

% PDF link settings
\hypersetup{
	colorlinks=true,
	linkcolor=LinkColor,
	citecolor=LinkColor,
	filecolor=LinkColor,
	menucolor=LinkColor,
	urlcolor=LinkColor,
	linktocpage=true,
	bookmarksnumbered=true
}

% Captions fontsize
\addtokomafont{caption}{\small}

% Bibliographie settings
\iflang{de}{
	\usepackage[
		backend=biber,		% recommended. Alternative: bibtex
		bibwarn=true,
		bibencoding=utf8,	% If .bib file is encoded with utf8, otherwise ascii
		sortlocale=de_DE,
		style=\quoteStyle,
	]{biblatex}
}
\iflang{en}{
	\usepackage[
		backend=biber,		% recommended. Alternative: bibtex
		bibwarn=true,
		bibencoding=utf8,	% If .bib file is encoded with utf8, otherwise ascii
		sortlocale=en_US,
		style=\quoteStyle,
	]{biblatex}
}

\addbibresource{../bibtex/sources.bib}

% Hurenkinder und Schusterjungen verhindern
% http://projekte.dante.de/DanteFAQ/Silbentrennung
\clubpenalty = 10000	% schließt Schusterjungen aus (Seitenumbruch nach der ersten Zeile eines neuen Absatzes)
\widowpenalty = 10000	% schließt Hurenkinder aus (die letzte Zeile eines Absatzes steht auf einer neuen Seite)
\displaywidowpenalty=10000

% Graphicspath
\graphicspath{{images/}{sourcecode/highlighted-out/}}

% frequently used programing languages
\lstloadlanguages{PHP,Python,Java,C,C++,bash}

\listingsettings{}
% Rename Listings
\renewcommand\lstlistingname{\listingPhrase}
\renewcommand\lstlistlistingname{\listListingPhrase}
\def\lstlistingautorefname{\authorListingPhrase}

% Spaces in tables
\setlength{\tabcolsep}{\tableColumnMargin}
\renewcommand{\arraystretch}{\tableRowMargin}

% Words not to be hyphenated
\hyphenation{
	Business-Entity
	Kon-zern-um-feld
	wei-ter-ent-wi-ckelt
	Pro-duk-tions-be-trieb
	he-run-ter-ge-la-den
}