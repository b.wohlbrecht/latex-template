$aux_dir					= '../../auxil';
$out_dir					= '../../out';
$do_cd						= 1;
$pdf_mode					= 1;
$show_time					= 1;
$pdflatex					= 'pdflatex -interaction=nonstopmode -file-line-error -synctex=1 %O %S';

add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('acn', 'acr', 0, 'run_makeglossaries');

sub run_makeglossaries {
  	# $_[0] is ../../auxil/main but needs to be main
	# TODO maybe use %R?
	my $arg = substr $_[0], 12;
	if ( $silent ) {
		system "makeglossaries -q -d $aux_dir $arg";
	}
	else {
		system "makeglossaries -d $aux_dir $arg";
	};
}

# For cleanup
push @generated_exts, 'glo', 'gls', 'glg';
push @generated_exts, 'acn', 'acr', 'alg';
push @generated_exts, 'bib';
$clean_ext .= ' %R.ist %R.xdy';